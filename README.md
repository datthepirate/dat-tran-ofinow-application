# dat-tran-ofinow-application

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run mock node server
```
node node-express-api.js
```