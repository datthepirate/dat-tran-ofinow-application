const express = require('express');
var cors = require('cors')
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const app = express();
const router = express.Router();

app.use(cors());

//db setup
const dbUsername = 'dbAdmin';
const dbPassword = 'w$0c0qxeM*f$Xwsg';
const dbUri = 'mongodb+srv://'+dbUsername+':'+dbPassword+'@cluster0-z6cro.mongodb.net/test?retryWrites=true';

mongoose.connect(dbUri, { useNewUrlParser: true }).then(
    () => {console.log('Database is connected') },
    err => { console.log('Can not connect to the database'+ err)}
);  
var userSchema = new mongoose.Schema({
    name: {type: String, required: true},
    password: {type: String, required: true},
    email: {type: String, required: true}
});
var User = mongoose.model('User', userSchema);

//api setup
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', (req, res) => {
    res.send('users')
});

router.route('/create').post(function (req,res){
    const user = new User(req.body);
    if(validatePassword(user.password)&&validateEmail(user.email)){
        user.save()
        .then(user => {
            res.json("User saved successfully.");
        })
        .catch(err => {
            res.status(400).send("Unable to save to database");
        })
    } else {
        res.status(400).send("Unable to save to database");
    }
});

router.route('/getAll').get(function (req,res){
    const user = new User(req.body);
    User.find({},function(err, users){
        if(err){
            res.status(400).send("Unable to connect to database");
        } else {
            res.json(users);
        }
    });
});

app.use('/users',router);

app.listen(8000, () => {
    User.find({},function(err, users){
        console.log(users);
    });
    console.log('Example app listening on port 8000!')
});

//help functions to validate
function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}
function validatePassword(password) {
    return password.length > 7 && password.length < 27;
}
