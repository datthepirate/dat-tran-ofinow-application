import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    registration: 'noSubmit',
    page: 'registration'
  },
  mutations: {
    registrationChange(state, registration){
      state.registration = registration;
    },
    nav(state, page){
      state.page = page;
    }
  },
  actions: {

  },
  getters: {
    registration: state => state.registration,
    page: state => state.page
  }
});
